//============================================================================
// Name        : sortowanie.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <time.h>
#include <iomanip>
using namespace std;

void show_array_elements(int *A,int size);
void insertSort(int *A,int size);
void merge(int A[], int p, int q, int r);

int main() {

int start;
cout<<"Enter start number of your array "<<endl;
cin>>start;
int end;

cout<<"Enter end number of your array "<<endl;
cin>>end;
int size=end-start;

int A[size];

for (int i=0;i<size;i++){
	int temp=rand()%(size)+start;
	A[i]=temp;
//	cout<<A[i]<<endl;
}

cout<<"SIZE"<<sizeof(A) / sizeof(A[0]);
//-------------------insertion sort
clock_t st=clock();

//insertSort(A,size);
show_array_elements(A,size);
merge(A, 0, size/2, size);
show_array_elements(A,size);

cout<<"time taken"<<fixed<<setprecision(3)<<(static_cast<double>(clock())-st/ CLOCKS_PER_SEC)<<endl;/*(1.0*clock()-st) / CLOCKS_PER_SEC<<endl;*/


}

void show_array_elements(int A[],int size){
	for(int i=0;i<size;i++){
		cout<<i<<" = " << A[i]<<endl;
	}
}

void insertSort(int *A,int size){
	int key, i, j;
	for (i=2;i<size;i++){ //wykonanie n razy
		key=A[i];
		j=i-1;
		while (j>=0 && A[j] > key){
			A[j+1]=A[j];
			j=j-1;
		}
		A[j+1]=key;
	}
}

void merge(int A[], int p, int q, int r)//p-pierwszy element młodszego opdzbioru, q-pierwszy element starszego podzbioru, r-ostani element starszego podzbioru
{
	int n1=q-p+1;
	int n2=r-q;
	int L[n1];
	int R[n2];
	for(int i=1;i<n1;i++)
	{
		L[i]=A[p+i-1];
	}
	for (int j=1;j<n2;j++)
	{
		 R[j]=A[q+j];
	}
	int i=1;
	int j=1;
	for (int k=p;k<r;k++)
	{
		if (L[i]<=R[j])
		{
			A[k]=L[i];
			i=i+1;
		}
		else
			A[k]=R[j];
			j=j+1;
	}
}


